import unittest

from bongo_coding.problem_1 import get_dict_depth


class ProblemTestCase(unittest.TestCase):
    def test_get_dict_depth_1(self):
        data = {"key1": 1, "key2": {"key3": 3, "key4": 4}}
        actual = get_dict_depth(data)
        expected = ["key1 1", "key2 1", "key3 2", "key4 2"]
        self.assertListEqual(actual, expected)

    def test_get_dict_depth_2(self):
        data = {"key1": 1, "key2": {"key3": 1, "key4": {"key5": 4}}}
        actual = get_dict_depth(data)
        expected = ["key1 1", "key2 1", "key3 2", "key4 2", "key5 3"]
        self.assertListEqual(actual, expected)

    def test_get_dict_depth_3(self):
        data = {
            "key1": 1,
            "key2": {"key3": 3, "key4": {"key5": {"key6": 6}}},
            "key7": 7,
        }
        actual = get_dict_depth(data)
        expected = [
            "key1 1",
            "key2 1",
            "key7 1",
            "key3 2",
            "key4 2",
            "key5 3",
            "key6 4",
        ]
        self.assertListEqual(actual, expected)
