from collections import deque
from typing import Deque, Dict, List


def get_dict_depth(data: Dict) -> List[str]:
    queue: Deque[Dict] = deque()
    result: List[str] = list()
    depth: int = 1
    for key, val in data.items():
        if isinstance(val, dict):
            queue.append(val)
        result.append(f"{key} {depth}")
    while queue:
        d: Dict = queue.popleft()
        depth += 1
        for key, val in d.items():
            result.append(f"{key} {depth}")
            if isinstance(val, dict):
                queue.append(val)
    return result


if __name__ == "__main__":
    items: Dict = {"key1": 1, "key2": {"key3": 1, "key4": {"key5": 4}}}
    for item in get_dict_depth(items):
        print(item)
