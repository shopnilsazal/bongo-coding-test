from __future__ import annotations

from collections import deque
from typing import Any, Deque, Dict, List, Union


class Point:
    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y


class Person:
    def __init__(self, first_name: str, last_name: str, father: Union[Person, None]):
        self.first_name = first_name
        self.last_name = last_name
        self.father = father


def get_class_name(cls: Any) -> str:
    return cls.__class__.__name__


def get_object_depth(data: Dict) -> List[str]:
    object_list: List[str] = list(globals())
    queue: Deque = deque()
    result: List[str] = list()
    depth: int = 1
    for key, val in data.items():
        if isinstance(val, dict) or (get_class_name(val) in object_list):
            queue.append(val)
        result.append(f"{key} {depth}")
    while queue:
        d = queue.popleft()
        depth += 1
        if isinstance(d, dict) or (get_class_name(d) in object_list):
            if get_class_name(d) in object_list:
                d = d.__dict__
            for key, val in d.items():
                result.append(f"{key} {depth}")
                if isinstance(val, dict) or (get_class_name(val) in object_list):
                    queue.append(val)
    return result


if __name__ == "__main__":
    person_a = Person("User", "Name", None)
    person_b = Person("User", "Name", person_a)
    items = {
        "key1": 1,
        "key2": {"key3": 1, "key4": {"key5": 4, "user": person_b,}},
    }

    for item in get_object_depth(items):
        print(item)
