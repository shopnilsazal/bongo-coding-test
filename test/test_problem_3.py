import unittest

from bongo_coding.problem_3 import Node, get_lca


class ProblemTestCase(unittest.TestCase):
    def setUp(self):
        self.n1: Node = Node(1, None)
        self.n2: Node = Node(2, self.n1)
        self.n3: Node = Node(3, self.n1)
        self.n4: Node = Node(4, self.n2)
        self.n5: Node = Node(5, self.n2)
        self.n6: Node = Node(6, self.n3)
        self.n7: Node = Node(7, self.n3)
        self.n8: Node = Node(8, self.n4)
        self.n9: Node = Node(9, self.n4)

    def test_get_lca_1(self):
        actual: int = get_lca(self.n6, self.n7)
        expected: int = 3
        self.assertEqual(actual, expected)

    def test_get_lca_2(self):
        actual: int = get_lca(self.n3, self.n7)
        expected: int = 3
        self.assertEqual(actual, expected)

    def test_get_lca_3(self):
        actual: int = get_lca(self.n5, self.n8)
        expected: int = 2
        self.assertEqual(actual, expected)

    def test_get_lca_4(self):
        actual: int = get_lca(self.n3, self.n4)
        expected: int = 1
        self.assertEqual(actual, expected)
