from __future__ import annotations

from typing import Set, Union


class Node:
    def __init__(self, value: int, parent: Union[Node, None]):
        self.value = value
        self.parent = parent


def get_lca(node_1: Node, node_2: Node) -> int:
    visited_nodes: Set[int] = set()
    current_node: Node = node_1
    while current_node.parent is not None:
        visited_nodes.add(current_node.value)
        current_node = current_node.parent

    current_node = node_2
    while current_node.value not in visited_nodes and current_node.parent is not None:
        current_node = current_node.parent

    return current_node.value


if __name__ == "__main__":
    n1: Node = Node(1, None)
    n2: Node = Node(2, n1)
    n3: Node = Node(3, n1)
    n4: Node = Node(4, n2)
    n5: Node = Node(5, n2)
    n6: Node = Node(6, n3)
    n7: Node = Node(7, n3)
    n8: Node = Node(8, n4)
    n9: Node = Node(9, n4)

    print(get_lca(n6, n7))
    print(get_lca(n3, n7))
