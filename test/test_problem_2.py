from __future__ import annotations

import unittest

from bongo_coding.problem_2 import Person, Point, get_class_name, get_object_depth


class ProblemTestCase(unittest.TestCase):
    def test_get_class_name_1(self):
        data = 5
        actual = get_class_name(data)
        expected = "int"
        self.assertEqual(actual, expected)

    def test_get_class_name_2(self):
        data = Person("User", "Name", None)
        actual = get_class_name(data)
        expected = "Person"
        self.assertEqual(actual, expected)

    def test_get_class_name_3(self):
        data = Point(3, 5)
        actual = get_class_name(data)
        expected = "Point"
        self.assertEqual(actual, expected)

    def test_get_object_depth_1(self):
        data = {"key1": 1, "key2": {"key3": 1, "key4": {"key5": 4}}}
        actual = get_object_depth(data)
        expected = ["key1 1", "key2 1", "key3 2", "key4 2", "key5 3"]
        self.assertListEqual(actual, expected)

    def test_get_object_depth_2(self):
        p: Point = Point(3, 5)
        data = {"key1": 1, "key2": {"key3": 3, "point": p}}
        actual = get_object_depth(data)
        expected = ["key1 1", "key2 1", "key3 2", "point 2", "x 3", "y 3"]
        self.assertListEqual(actual, expected)

    def test_get_object_depth_3(self):
        person_a = Person("User", "Name", None)
        person_b = Person("User", "Name", person_a)
        data = {
            "key1": 1,
            "key2": {"key3": 1, "key4": {"key5": 4, "user": person_b,}},
        }
        actual = get_object_depth(data)
        expected = [
            "key1 1",
            "key2 1",
            "key3 2",
            "key4 2",
            "key5 3",
            "user 3",
            "first_name 4",
            "last_name 4",
            "father 4",
            "first_name 5",
            "last_name 5",
            "father 5",
        ]
        self.assertListEqual(actual, expected)
