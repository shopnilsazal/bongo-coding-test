# Bongo Coding Test

## Setup

This project requires `python3.6+` to test. Also it uses `pipenv` to manage dependencies. First clone the repo and `cd` into project directory.

```sh
# If you do not have pipenv installed
pip3 install pipenv


# Install dependencies
pipenv --python 3
pipenv install --dev --skip-lock


# Setup pre-commit and pre-push hooks 
# (it's not needed unless you want to check it)
pipenv run pre-commit install -t pre-commit
pipenv run pre-commit install -t pre-push
```

## Testing

To see the output of a specific problem with given test data in problem set.
 
```sh
# Problem 1
pipenv run python bongo_coding/problem_1.py

# Problem 2
pipenv run python bongo_coding/problem_2.py

# Problem 3
pipenv run python bongo_coding/problem_3.py
```

To see the unit testing result.
```sh
pipenv run pytest
```
